#!/usr/bin/python
import sys
import math
from odf.opendocument import OpenDocumentSpreadsheet, load
from odf.style import TextProperties
from odf.table import Table, TableColumn, TableRow, TableCell
from odf.text import H, P, Span

from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5 import QtCore

filename = "table.png"

def QNearPoint(point,pos):
    return QtCore.QRect(point[0]-8,point[1]-8,16,16).contains(pos)

class TableArea(QtWidgets.QLabel):
    def __init__(self, points, textWidget, parent=None):
        super(TableArea,self).__init__(parent)
        self.selected = None
        self.moving = False
        self.points = points
        self.tableImage = QtGui.QImage(filename)
        self.width = self.tableImage.width()
        self.height = self.tableImage.height()
        self.createPixmaps()
        self.updatePixmap()
        self.setMouseTracking(True)
        self.tagText = textWidget
        self.tagText.textEdited.connect(self.textEvent)
    def textEvent(self, text):
        if (self.selected):
            self.points[self.selected]["tags"] = text
    def mousePressEvent(self, event):
        if (event.button() == QtCore.Qt.LeftButton and self.selected and QNearPoint(self.selected,event.pos())):
            self.moving = True
    def mouseMoveEvent(self, event):
        if (self.moving and self.selected):
            newPoint = (event.x(),event.y())
            self.points[newPoint] = self.points[self.selected]
            for linkedPoint in self.points:
                if self.selected in self.points[linkedPoint]["links"]:
                    self.points[linkedPoint]["links"].remove(self.selected)
                    self.points[linkedPoint]["links"].append(newPoint)
            del self.points[self.selected]
            self.selected = newPoint
            self.updatePixmap()
    def mouseReleaseEvent(self, event):
        if (self.moving):
            self.moving = False
        if (event.button() == QtCore.Qt.RightButton):
            point = {}
            point["links"] = []
            point["tags"] = ""
            self.points[(event.x(),event.y())] = point
        elif (event.button() == QtCore.Qt.LeftButton):
                for i in self.points:
                    if QNearPoint(i,event.pos()):
                        self.selected = i
                        self.tagText.setText(self.points[i]["tags"])
                        break
        elif (event.button() == QtCore.Qt.MidButton and self.selected):
            for i in self.points:
                if QNearPoint(i,event.pos()):
                    if i in self.points[self.selected]["links"]:
                        self.points[i]["links"].remove(self.selected)
                        self.points[self.selected]["links"].remove(i)
                    elif  self.selected != i:
                        self.points[self.selected]["links"].append(i)
                        self.points[i]["links"].append(self.selected)
                    break
        self.updatePixmap()
    def createPixmaps(self):
        # Table
        self.tablePixmap = QtGui.QPixmap()
        self.tablePixmap.convertFromImage(self.tableImage)
        # Point
        self.pointPixmap = QtGui.QPixmap(16,16)
        self.pointPixmap.fill(QtGui.QColor(0,255,0,255))
        painter = QtGui.QPainter(self.pointPixmap)
        painter.setPen(QtGui.QPen(QtGui.QColor(255,255,255,255)))
        painter.drawRect(QtCore.QRect(0,0,16,16))
        # Point selectionne
        self.selectedPixmap = QtGui.QPixmap(16,16)
        self.selectedPixmap.fill(QtGui.QColor(255,255,255,255))
        painter = QtGui.QPainter(self.selectedPixmap)
        painter.setPen(QtGui.QPen(QtGui.QColor(255,255,255,255)))
        painter.drawRect(QtCore.QRect(0,0,16,16))
        # Point lie
        self.linkedPixmap = QtGui.QPixmap(16,16)
        self.linkedPixmap.fill(QtGui.QColor(255,0,0,255))
        painter = QtGui.QPainter(self.linkedPixmap)
        painter.setPen(QtGui.QPen(QtGui.QColor(0,0,0,255)))
        painter.drawRect(QtCore.QRect(0,0,16,16))
    def updatePixmap(self):
        self.tablePixmap.convertFromImage(self.tableImage)
        painter = QtGui.QPainter(self.tablePixmap)
        for i in self.points:
            qRect = QtCore.QRect(i[0]-8,i[1]-8,16,16)
            if self.selected and self.selected == i:
                painter.drawPixmap(qRect,self.selectedPixmap)
                painter.setPen(QtGui.QPen(QtGui.QColor(220,180,180,220)))
            elif self.selected and i in self.points[self.selected]["links"]:
                painter.drawPixmap(qRect,self.linkedPixmap)
                painter.setPen(QtGui.QPen(QtGui.QColor(220,180,180,220)))
            else:
                painter.drawPixmap(qRect,self.pointPixmap)
                painter.setPen(QtGui.QPen(QtGui.QColor(180,180,180,180)))
            for j in self.points[i]["links"]:
                painter.drawLine(i[0],i[1],j[0],j[1])
        self.setPixmap(self.tablePixmap)

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow,self).__init__(parent)
        self.points = {}
        self.tagText = QtWidgets.QLineEdit()
        self.imageArea = TableArea(self.points, self.tagText)
        self.setWindowTitle(filename)
        self.setCentralWidget(self.imageArea)
        self.menubarAction = []
        self.addAction(self.menuBar(), "&Load", self.load)
        self.addAction(self.menuBar(), "&Clear", self.clear)
        self.addAction(self.menuBar(), "&Save", self.save)
        self.menuSelected = self.menuBar().addMenu('&Selected')
        self.addAction(self.menuSelected, "Delete", self.delete)
        self.statusBar().addPermanentWidget(self.tagText)
    def keyPressEvent(self, event):
        if (event.key() == QtCore.Qt.Key_Delete):
            self.delete()
    def addAction(self, menu, title, result):
        action = QtWidgets.QAction(title, menu)
        action.triggered.connect(result)
        menu.addAction(action)
    def clear(self):
        print("Clearing map.")
        self.imageArea.points.clear()
        self.imageArea.updatePixmap()
    def save(self):
        filename = QtWidgets.QFileDialog.getSaveFileName(self, "Save points file", "./","*.ods")[0]
        document = OpenDocumentSpreadsheet()
        pointListTable = Table(name="Checkpoints")
        matrixTable = Table(name="Links")
        k=1
        matrixList = []
        for point in self.points:
            matrixList.append(point)
            tr = TableRow()
            x = round(point[0] * 3000 / self.imageArea.width)
            y = round(point[1] * 2000 / self.imageArea.height)
            tr.addElement(TableCell(valuetype = "float", value=x))
            tr.addElement(TableCell(valuetype = "float", value=y))
            tc = TableCell()
            p = P()
            p.addText(self.points[point]["tags"])
            tc.addElement(p)
            tr.addElement(tc)
            pointListTable.addElement(tr)
            k += 1
        for point in matrixList:
            tr = TableRow()
            for otherPoint in matrixList:
                if otherPoint in self.points[point]["links"]:
                    tr.addElement(TableCell(valuetype = "float", value=1))
                else:
                    tr.addElement(TableCell(valuetype = "float", value=0))
            matrixTable.addElement(tr)
        document.spreadsheet.addElement(pointListTable)
        document.spreadsheet.addElement(matrixTable)
        document.save(filename)
        print("Saved as " + filename)
    def load(self):
        filePicked = QtWidgets.QFileDialog.getOpenFileName(self,"Load file with points","./","*.ods")[0]
        docRead = load(filePicked)
        if docRead:
            self.points.clear()
            pointListTable = docRead.spreadsheet.getElementsByType(Table)[0]
            matrixTable = docRead.spreadsheet.getElementsByType(Table)[1]
            matrixList = []
            for row in pointListTable.getElementsByType(TableRow):
                point = {}
                point["links"] = []
                cells = row.getElementsByType(TableCell)
                x = round(int(cells[0].getAttribute("value")) * self.imageArea.width / 3000)
                y = round(int(cells[1].getAttribute("value")) * self.imageArea.height / 2000)
                point["tags"] = str(cells[2].getElementsByType(P)[0])
                self.points[(x,y)] = point
                matrixList.append((x,y))
            matrixRows = matrixTable.getElementsByType(TableRow)
            for rowNumber in range(len(matrixRows)):
                rowCells = matrixRows[rowNumber].getElementsByType(TableCell)
                for cellNumber in range(len(rowCells)):
                    if int(rowCells[cellNumber].getAttribute("value")) == 1:
                        self.points[matrixList[rowNumber]]["links"].append(matrixList[cellNumber])
                        self.points[matrixList[cellNumber]]["links"].append(matrixList[rowNumber])
            self.imageArea.selected = None
            print("Loaded " + filePicked)
            self.tagText.clear()
            self.imageArea.updatePixmap()
    def delete(self):
        if self.imageArea.selected:
            for p in self.points[self.imageArea.selected]["links"]:
                self.points[p]["links"].remove(self.imageArea.selected)
            del self.points[self.imageArea.selected]
            self.imageArea.selected = None
            self.tagText.clear()
            self.imageArea.updatePixmap()

app = QtWidgets.QApplication(sys.argv)
window = MainWindow()

window.show()
sys.exit(app.exec_())
